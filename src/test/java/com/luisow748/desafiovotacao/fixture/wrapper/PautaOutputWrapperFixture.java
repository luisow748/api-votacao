package com.luisow748.desafiovotacao.fixture.wrapper;


import com.luisow748.desafiovotacao.domain.Pauta;
import com.luisow748.desafiovotacao.service.output.PautaOutputWrapper;

public class PautaOutputWrapperFixture {
    public static PautaOutputWrapper getFromPauta(Pauta pauta) {
        PautaOutputWrapper pautaOutputWrapper = new PautaOutputWrapper();
        pautaOutputWrapper.setPautaId(pauta.getPautaId());
        pautaOutputWrapper.setDescricao(pauta.getDescricao());
        pautaOutputWrapper.setNome(pauta.getNome());
        return pautaOutputWrapper;
    }
}
