package com.luisow748.desafiovotacao.resource;


import com.fasterxml.jackson.databind.ObjectMapper;
import com.luisow748.desafiovotacao.domain.Sessao;
import com.luisow748.desafiovotacao.fixture.SessaoFixture;
import com.luisow748.desafiovotacao.fixture.wrapper.SessaoInputWrapperFixture;
import com.luisow748.desafiovotacao.fixture.wrapper.SessaoOutputWrapperFixture;
import com.luisow748.desafiovotacao.resource.mapper.SessaoMapper;
import com.luisow748.desafiovotacao.service.impl.SessaoService;
import com.luisow748.desafiovotacao.service.input.SessaoInputWrapper;
import com.luisow748.desafiovotacao.service.output.SessaoOutputWrapper;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

import static org.mockito.ArgumentMatchers.any;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@WebMvcTest(SessaoResource.class)
class SessaoResourceTest {
    @MockBean
    SessaoService sessaoService;
    @MockBean
    SessaoMapper sessaoMapper;

    @Autowired
    MockMvc mockMvc;
    private static final ObjectMapper mapper = new ObjectMapper();

    @Test
    void DeveIniciarSessao() throws Exception {
        Sessao sessao = SessaoFixture.getWithId(1);
        SessaoInputWrapper sessaoInputWrapper = SessaoInputWrapperFixture.get();
        SessaoOutputWrapper sessaoOutputWrapper = SessaoOutputWrapperFixture.get();
        Mockito.when(sessaoService.iniciarSessao(any())).thenReturn(sessaoOutputWrapper);
        mockMvc.perform(post("/api/v1/sessao")
                        .content(mapper.writeValueAsString(sessaoInputWrapper))
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON)
                        .characterEncoding("utf-8"))
                .andExpect(status().isOk())
                .andDo(print())
                .andExpect(jsonPath("$.sessaoId", Matchers.is(1)))
                .andExpect(jsonPath("$.prazoDuracao", Matchers.is(2)))
                .andExpect(jsonPath("$.pautaId", Matchers.is("1")));
    }

}