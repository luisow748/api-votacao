package com.luisow748.desafiovotacao.exception;

public class SessaoInvalidaException extends RuntimeException {

    @Override
    public String getMessage() {
        return "Sessão inválida / finalizada";
    }
}