package com.luisow748.desafiovotacao.service.impl;


import com.luisow748.desafiovotacao.domain.Associado;
import com.luisow748.desafiovotacao.exception.RecursoInexistenteException;
import com.luisow748.desafiovotacao.repository.AssociadoRepository;
import com.luisow748.desafiovotacao.resource.mapper.AssociadoMapper;
import com.luisow748.desafiovotacao.service.input.AssociadoInputWrapper;
import com.luisow748.desafiovotacao.service.output.AssociadoOutputWrapper;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Objects;

@Service
@RequiredArgsConstructor
public class AssociadoService {
    private final AssociadoRepository associadoRepository;
    private final AssociadoMapper associadoMapper;

    public AssociadoOutputWrapper save(AssociadoInputWrapper associadoInputWrapper) {
        Associado associadoRecebido = associadoMapper.toEntity(associadoInputWrapper);
        Associado associadoSalvo = save(associadoRecebido);
        return associadoMapper.toOutputWrapper(associadoSalvo);
    }

    public Associado save(Associado associado) {
        return associadoRepository.save(associado);
    }

    public Associado findById(Integer associadoId) {
        return associadoRepository.findById(associadoId)
                .orElseThrow(() -> new RecursoInexistenteException("Associado inexistente"));
    }

    public Boolean isValid(Integer associadoId) {
        return Objects.nonNull(findById(associadoId));
    }
}
