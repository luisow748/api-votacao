package com.luisow748.desafiovotacao.resource;

import com.luisow748.desafiovotacao.service.impl.AssociadoService;
import com.luisow748.desafiovotacao.service.input.AssociadoInputWrapper;
import com.luisow748.desafiovotacao.service.output.AssociadoOutputWrapper;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("api/v1/associado")
@RequiredArgsConstructor
public class AssociadoResource {

    private final AssociadoService associadoService;

    @PostMapping
    ResponseEntity<AssociadoOutputWrapper> save(@RequestBody AssociadoInputWrapper associadoInputWrapper) {
        return ResponseEntity.ok(associadoService.save(associadoInputWrapper));
    }
}
