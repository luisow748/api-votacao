package com.luisow748.desafiovotacao.service.input;

import lombok.Data;
import lombok.RequiredArgsConstructor;
@Data
@RequiredArgsConstructor
public class AssociadoInputWrapper {

        private String nome;

}
