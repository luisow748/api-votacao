package com.luisow748.desafiovotacao.domain.enums;

import lombok.Getter;

@Getter
public enum ValorVotoEnum {
    SIM( "SIM"),
    NAO( "NAO");

    private final String valor;

    ValorVotoEnum(String descricao) {
        this.valor = descricao;
    }
}
