package com.luisow748.desafiovotacao.resource.mapper;

import com.luisow748.desafiovotacao.domain.Associado;
import com.luisow748.desafiovotacao.service.input.AssociadoInputWrapper;
import com.luisow748.desafiovotacao.service.output.AssociadoOutputWrapper;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface AssociadoMapper {
    Associado toEntity(AssociadoInputWrapper inputWrapper);
    AssociadoOutputWrapper toOutputWrapper(Associado associado);
}
