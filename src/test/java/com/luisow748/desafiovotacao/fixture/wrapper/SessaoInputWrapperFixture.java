package com.luisow748.desafiovotacao.fixture.wrapper;


import com.luisow748.desafiovotacao.service.input.SessaoInputWrapper;

public class SessaoInputWrapperFixture {

    public static SessaoInputWrapper get() {
        SessaoInputWrapper inputWrapper = new SessaoInputWrapper();
        inputWrapper.setPrazoDuracao(2);
        inputWrapper.setPautaId(1);
        return inputWrapper;
    }
}
