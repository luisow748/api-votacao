package com.luisow748.desafiovotacao.resource;


import com.luisow748.desafiovotacao.service.impl.PautaService;
import com.luisow748.desafiovotacao.service.input.PautaInputWrapper;
import com.luisow748.desafiovotacao.service.output.PautaOutputWrapper;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("api/v1/pauta")
@AllArgsConstructor
public class PautaResource {
    private final PautaService pautaService;

    @PostMapping
    ResponseEntity<PautaOutputWrapper> save(@RequestBody PautaInputWrapper pautaInput) {
        return ResponseEntity.ok(pautaService.save(pautaInput));
    }
}
