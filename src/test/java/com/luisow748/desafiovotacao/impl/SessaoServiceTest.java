package com.luisow748.desafiovotacao.impl;


import com.luisow748.desafiovotacao.domain.Sessao;
import com.luisow748.desafiovotacao.exception.RecursoInexistenteException;
import com.luisow748.desafiovotacao.fixture.SessaoFixture;
import com.luisow748.desafiovotacao.fixture.wrapper.SessaoInputWrapperFixture;
import com.luisow748.desafiovotacao.repository.SessaoRepository;
import com.luisow748.desafiovotacao.service.impl.SessaoService;
import com.luisow748.desafiovotacao.service.input.SessaoInputWrapper;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class SessaoServiceTest {
    @InjectMocks
    SessaoService sessaoService;

    @Mock
    SessaoRepository sessaoRepository;

    @Test
    void DeveIniciarSessao(){
        Sessao sessao = SessaoFixture.get();
        when(sessaoRepository.save(any())).thenReturn(sessao);
        SessaoInputWrapper sessaoInputWrapper = SessaoInputWrapperFixture.get();
        Sessao sessaoIniciada = sessaoService.iniciar(sessaoInputWrapper);
        assertEquals(10, sessaoIniciada.getPrazoDuracao());
        assertEquals(1, sessaoIniciada.getSessaoId());
    }
    @Test
    void NaoDeveRetornarSessaoPorId() {
        assertThrows(RecursoInexistenteException.class, () -> sessaoService.findById(1));
    }

    @Test
    void DeveRetornarSessaoPorId() {
        Sessao sessao = SessaoFixture.get();
        when(sessaoRepository.findById(any())).thenReturn(Optional.of(sessao));
        Sessao sessaoRetornada = sessaoService.findById(1);
        assertEquals(10, sessaoRetornada.getPrazoDuracao());
        assertEquals(1, sessaoRetornada.getSessaoId());
    }

    @Test
    void DeveVerificarSessaoValida() {
        Sessao sessao = SessaoFixture.get();
        when(sessaoRepository.findById(any())).thenReturn(Optional.of(sessao));
        Boolean isSessaoValida = sessaoService.isValid(1);
        assertEquals(true, isSessaoValida);
    }

    @Test
    void DeveVerificarSessaoExpirada() {
        Sessao sessao = SessaoFixture.getExpirada();
        when(sessaoRepository.findById(any())).thenReturn(Optional.of(sessao));
        Boolean isSessaoValida = sessaoService.isValid(1);
        assertEquals(false, isSessaoValida);
    }
}
