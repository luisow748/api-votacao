package com.luisow748.desafiovotacao.resource.mapper;


import com.luisow748.desafiovotacao.domain.Pauta;
import com.luisow748.desafiovotacao.service.input.PautaInputWrapper;
import com.luisow748.desafiovotacao.service.output.PautaOutputWrapper;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring")
public interface PautaMapper {

    @Mapping(target = "pautaId", ignore = true)
    @Mapping(target = "sessao", ignore = true)
    Pauta toEntity(PautaInputWrapper pautaInputWrapper);

    PautaOutputWrapper toOutputWrapper(Pauta pauta);
}
