package com.luisow748.desafiovotacao.fixture.wrapper;


import com.luisow748.desafiovotacao.service.input.PautaInputWrapper;

public class PautaInputWrapperFixture {

    public static PautaInputWrapper get() {
        PautaInputWrapper pautaInputWrapper = new PautaInputWrapper();
        pautaInputWrapper.setDescricao("Descrição");
        pautaInputWrapper.setNome("Nome");
        return pautaInputWrapper;
    }
}
