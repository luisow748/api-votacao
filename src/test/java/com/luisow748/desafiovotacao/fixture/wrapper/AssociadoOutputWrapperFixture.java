package com.luisow748.desafiovotacao.fixture.wrapper;


import com.luisow748.desafiovotacao.service.output.AssociadoOutputWrapper;

public class AssociadoOutputWrapperFixture {

    public static AssociadoOutputWrapper get() {
        AssociadoOutputWrapper associadoOutputWrapper = new AssociadoOutputWrapper();
        associadoOutputWrapper.setAssociadoId(1);
        associadoOutputWrapper.setNome("Associado");
        return associadoOutputWrapper;
    }
}
