package com.luisow748.desafiovotacao.repository;


import com.luisow748.desafiovotacao.domain.Associado;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AssociadoRepository extends JpaRepository<Associado, Integer> {}
