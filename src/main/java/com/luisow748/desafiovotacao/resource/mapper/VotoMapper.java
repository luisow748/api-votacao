package com.luisow748.desafiovotacao.resource.mapper;

import com.luisow748.desafiovotacao.domain.Voto;
import com.luisow748.desafiovotacao.service.output.VotoOutputWrapper;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel="spring")
public interface VotoMapper {

    @Mapping(source = "sessao.sessaoId", target = "sessaoId")
    @Mapping(source = "pauta.pautaId", target = "pautaId")
    VotoOutputWrapper toOutputWrapper(Voto voto);
}
