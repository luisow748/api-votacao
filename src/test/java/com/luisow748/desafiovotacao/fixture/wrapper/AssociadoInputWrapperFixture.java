package com.luisow748.desafiovotacao.fixture.wrapper;


import com.luisow748.desafiovotacao.service.input.AssociadoInputWrapper;

public class AssociadoInputWrapperFixture {

    public static AssociadoInputWrapper get() {
        AssociadoInputWrapper associadoInputWrapper = new AssociadoInputWrapper();
        associadoInputWrapper.setNome("Associado");
        return associadoInputWrapper;
    }
}
