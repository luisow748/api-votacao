package com.luisow748.desafiovotacao.service.impl;


import com.luisow748.desafiovotacao.domain.Voto;
import com.luisow748.desafiovotacao.domain.enums.ValorVotoEnum;
import com.luisow748.desafiovotacao.exception.RecursoInexistenteException;
import com.luisow748.desafiovotacao.exception.SessaoInvalidaException;
import com.luisow748.desafiovotacao.repository.VotoRepository;
import com.luisow748.desafiovotacao.resource.mapper.VotoMapper;
import com.luisow748.desafiovotacao.service.input.VotoInputWrapper;
import com.luisow748.desafiovotacao.service.output.ContabilizacaoOutputWrapper;
import com.luisow748.desafiovotacao.service.output.VotoOutputWrapper;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Objects;

@Service
@RequiredArgsConstructor
public class VotoService {
    public static final String PAUTA_APROVADA = "Pauta Aprovada.";
    public static final String PAUTA_REPROVADA = "Pauta Reprovada.";
    public static final String EMPATE_DE_VOTOS = "Empate de votos";
    private final VotoRepository votoRepository;
    private final PautaService pautaService;
    private final SessaoService sessaoService;
    private final AssociadoService associadoService;
    private final VotoMapper votoMapper;

    public VotoOutputWrapper saveVoto(VotoInputWrapper votoInputWrapper) {
        Voto votoSalvo = save(votoInputWrapper);
        return votoMapper.toOutputWrapper(votoSalvo);
    }

    public Voto save(VotoInputWrapper votoInput) {
        if (isValid(votoInput)) {
            Voto novoVoto = Voto.builder()
                    .valor(votoInput.getValor())
                    .pauta(pautaService.findById(votoInput.getPautaId()))
                    .sessao(sessaoService.findById(votoInput.getSessaoId()))
                    .associado(associadoService.findById(votoInput.getAssociadoId()))
                    .build();
            return votoRepository.save(novoVoto);
        }
        throw new SessaoInvalidaException();
    }

    public List<Voto> findBySessao(Integer sessaoId) {
        ArrayList<Voto> votosPorSessao = new ArrayList<>(votoRepository.findBySessaoSessaoId(sessaoId)
                .orElseThrow(() -> new RecursoInexistenteException("Não há votos para essa sessão")));
        votosPorSessao.sort(Comparator.comparing(Voto::getValor));
        return votosPorSessao;
    }

    private boolean isAssociadoVotouNaSessao(Integer associadoId, Integer sessaoId) {
        return votoRepository.findByAssociadoAssociadoIdAndSessaoSessaoId(associadoId, sessaoId).isPresent();
    }

    public Boolean isValid(VotoInputWrapper votoInput) {
        boolean isPautaExists = Objects.nonNull((votoInput.getPautaId()));
        boolean isValidSessao = sessaoService.isValid(votoInput.getSessaoId());
        boolean isValidAssociado = associadoService.isValid(votoInput.getAssociadoId());
        boolean associadoVotouNaSessao = isAssociadoVotouNaSessao(votoInput.getAssociadoId(), votoInput.getSessaoId());
        boolean isValorCorreto = isValorCorreto(votoInput);
        return isPautaExists && isValidSessao && isValidAssociado && isValorCorreto && (!associadoVotouNaSessao);
    }

    private Boolean isValorCorreto(VotoInputWrapper votoInput) {
        String valorVoto = votoInput.getValor().toUpperCase().trim();
        return valorVoto.equals(ValorVotoEnum.SIM.getValor()) || valorVoto.equals(ValorVotoEnum.NAO.getValor());
    }

    public ContabilizacaoOutputWrapper getContabilizacao(Integer sessaoId) {
        var votosValidos = findBySessao(sessaoId);
        int votosSim = getQuantidadeVotos(votosValidos, ValorVotoEnum.SIM);
        int votosNao = getQuantidadeVotos(votosValidos, ValorVotoEnum.NAO);
        String resultado = getResultadoVotos(votosSim, votosNao);
        return ContabilizacaoOutputWrapper.builder()
                .votosSim(votosSim)
                .votosNao(votosNao)
                .resultado(resultado)
                .build();
    }

    public static int getQuantidadeVotos(List<Voto> votosValidos, ValorVotoEnum valor) {
        return (int) votosValidos.stream().filter(voto -> voto.getValor().toUpperCase().equals(valor.getValor())).count();
    }

    private static String getResultadoVotos(int votosSim, int votosNao) {
        String resultado;
        if (votosSim > votosNao) {
            resultado = PAUTA_APROVADA;
        } else if (votosSim < votosNao) {
            resultado = PAUTA_REPROVADA;
        } else {
            resultado = EMPATE_DE_VOTOS;
        }
        return resultado;
    }
}
