package com.luisow748.desafiovotacao.fixture.wrapper;


import com.luisow748.desafiovotacao.service.output.SessaoOutputWrapper;

import java.time.LocalDateTime;

public class SessaoOutputWrapperFixture {
    public static SessaoOutputWrapper get() {
        SessaoOutputWrapper sessaoOutputWrapper = new SessaoOutputWrapper();
        sessaoOutputWrapper.setSessaoId(1);
        sessaoOutputWrapper.setInicioSessao(LocalDateTime.now());
        sessaoOutputWrapper.setTerminoSessao(LocalDateTime.now().plusMinutes(2));
        sessaoOutputWrapper.setPrazoDuracao(2);
        sessaoOutputWrapper.setPautaId("1");
        return sessaoOutputWrapper;
    }
}
