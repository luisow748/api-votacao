package com.luisow748.desafiovotacao.impl;


import com.luisow748.desafiovotacao.domain.Voto;
import com.luisow748.desafiovotacao.domain.enums.ValorVotoEnum;
import com.luisow748.desafiovotacao.fixture.VotoFixture;
import com.luisow748.desafiovotacao.fixture.wrapper.VotoInputWrapperFixture;
import com.luisow748.desafiovotacao.repository.VotoRepository;
import com.luisow748.desafiovotacao.service.impl.AssociadoService;
import com.luisow748.desafiovotacao.service.impl.SessaoService;
import com.luisow748.desafiovotacao.service.impl.VotoService;
import com.luisow748.desafiovotacao.service.input.VotoInputWrapper;
import com.luisow748.desafiovotacao.service.output.ContabilizacaoOutputWrapper;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;


import static com.luisow748.desafiovotacao.service.impl.VotoService.*;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class VotoServiceTest {

    @InjectMocks
    VotoService votoService;

    @Mock
    VotoRepository votoRepository;
    @Mock
    SessaoService sessaoService;
    @Mock
    AssociadoService associadoService;

    @Test
    void DeveVerificarVotoValido() {
        VotoInputWrapper votoInput = VotoInputWrapperFixture.get(ValorVotoEnum.NAO.getValor());
        when(sessaoService.isValid(any())).thenReturn(true);
        when(associadoService.isValid(any())).thenReturn(true);
        Boolean isValid = votoService.isValid(votoInput);
        assertEquals(true, isValid);
    }

    @Test
    void NaoDevePermitirVotoAssociadoQueJaVotou() {
        VotoInputWrapper votoInput = VotoInputWrapperFixture.get(ValorVotoEnum.NAO.getValor());
        when(sessaoService.isValid(any())).thenReturn(true);
        when(associadoService.isValid(any())).thenReturn(true);
        when(votoRepository.findByAssociadoAssociadoIdAndSessaoSessaoId(any(), any())).thenReturn(Optional.of(VotoFixture.get(ValorVotoEnum.NAO.getValor())));
        Boolean isValid = votoService.isValid(votoInput);
        assertEquals(false, isValid);
    }

    @Test
    void NaoDevePermitirVotoComValorDiferenteDoAceito() {
        VotoInputWrapper votoInput = VotoInputWrapperFixture.get("Talvez");
        when(sessaoService.isValid(any())).thenReturn(true);
        when(associadoService.isValid(any())).thenReturn(true);
        when(votoRepository.findByAssociadoAssociadoIdAndSessaoSessaoId(any(), any())).thenReturn(Optional.empty());
        Boolean isValid = votoService.isValid(votoInput);
        assertEquals(false, isValid);
    }

    @Test
    void DevePermitirVotoComValorAceito() {
        VotoInputWrapper votoInput = VotoInputWrapperFixture.get(ValorVotoEnum.NAO.getValor());
        when(sessaoService.isValid(any())).thenReturn(true);
        when(associadoService.isValid(any())).thenReturn(true);
        when(votoRepository.findByAssociadoAssociadoIdAndSessaoSessaoId(any(), any())).thenReturn(Optional.empty());
        Boolean isValid = votoService.isValid(votoInput);
        assertEquals(true, isValid);
    }

    @Test
    void DeveRetornarContabilizacaoAprovada() {
        List<Voto> votos = new ArrayList<>();
        votos.add(VotoFixture.get(ValorVotoEnum.NAO.getValor()));
        votos.add(VotoFixture.get(ValorVotoEnum.SIM.getValor()));
        votos.add(VotoFixture.get(ValorVotoEnum.SIM.getValor()));
        votos.add(VotoFixture.get(ValorVotoEnum.SIM.getValor()));
        votos.add(VotoFixture.get(ValorVotoEnum.NAO.getValor()));
        when(votoRepository.findBySessaoSessaoId(any())).thenReturn(Optional.of(votos));
        ContabilizacaoOutputWrapper contabilizacao = votoService.getContabilizacao(1);
        assertEquals(PAUTA_APROVADA, contabilizacao.getResultado());
    }

    @Test
    void DeveRetornarContabilizacaoReprovada() {
        List<Voto> votos = new ArrayList<>();
        votos.add(VotoFixture.get(ValorVotoEnum.NAO.getValor()));
        votos.add(VotoFixture.get(ValorVotoEnum.NAO.getValor()));
        votos.add(VotoFixture.get(ValorVotoEnum.SIM.getValor()));
        votos.add(VotoFixture.get(ValorVotoEnum.NAO.getValor()));
        votos.add(VotoFixture.get(ValorVotoEnum.NAO.getValor()));
        when(votoRepository.findBySessaoSessaoId(any())).thenReturn(Optional.of(votos));
        ContabilizacaoOutputWrapper contabilizacao = votoService.getContabilizacao(1);
        assertEquals(PAUTA_REPROVADA, contabilizacao.getResultado());
    }

    @Test
    void DeveRetornarContabilizacaoEmpate() {
        List<Voto> votos = new ArrayList<>();
        votos.add(VotoFixture.get(ValorVotoEnum.SIM.getValor()));
        votos.add(VotoFixture.get(ValorVotoEnum.NAO.getValor()));
        votos.add(VotoFixture.get(ValorVotoEnum.SIM.getValor()));
        votos.add(VotoFixture.get(ValorVotoEnum.SIM.getValor()));
        votos.add(VotoFixture.get(ValorVotoEnum.NAO.getValor()));
        votos.add(VotoFixture.get(ValorVotoEnum.NAO.getValor()));
        when(votoRepository.findBySessaoSessaoId(any())).thenReturn(Optional.of(votos));
        ContabilizacaoOutputWrapper contabilizacao = votoService.getContabilizacao(1);
        assertEquals(EMPATE_DE_VOTOS, contabilizacao.getResultado());
    }

}