package com.luisow748.desafiovotacao.resource;


import com.luisow748.desafiovotacao.service.impl.VotoService;
import com.luisow748.desafiovotacao.service.input.VotoInputWrapper;
import com.luisow748.desafiovotacao.service.output.ContabilizacaoOutputWrapper;
import com.luisow748.desafiovotacao.service.output.VotoOutputWrapper;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("api/v1/voto")
@RequiredArgsConstructor
public class VotoResource {
    private final VotoService votoService;


    @PostMapping
    ResponseEntity<VotoOutputWrapper> save(@RequestBody VotoInputWrapper votoInputWrapper) {
        return ResponseEntity.ok(votoService.saveVoto(votoInputWrapper));
    }

    @GetMapping("{sessaoId}")
    ResponseEntity<ContabilizacaoOutputWrapper> getContabilizacao(@PathVariable Integer sessaoId) {
        return ResponseEntity.ok(votoService.getContabilizacao(sessaoId));
    }
}
