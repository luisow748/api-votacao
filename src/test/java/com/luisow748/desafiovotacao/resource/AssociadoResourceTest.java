package com.luisow748.desafiovotacao.resource;


import com.fasterxml.jackson.databind.ObjectMapper;
import com.luisow748.desafiovotacao.domain.Associado;
import com.luisow748.desafiovotacao.fixture.AssociadoFixture;
import com.luisow748.desafiovotacao.fixture.wrapper.AssociadoInputWrapperFixture;
import com.luisow748.desafiovotacao.fixture.wrapper.AssociadoOutputWrapperFixture;
import com.luisow748.desafiovotacao.service.impl.AssociadoService;
import com.luisow748.desafiovotacao.service.input.AssociadoInputWrapper;
import com.luisow748.desafiovotacao.service.output.AssociadoOutputWrapper;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

import static org.mockito.ArgumentMatchers.any;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@WebMvcTest(AssociadoResource.class)
class AssociadoResourceTest {

    @MockBean
    AssociadoService associadoService;
    @Autowired
    MockMvc mockMvc;
    private static final ObjectMapper mapper = new ObjectMapper();

    @Test
    void DeveSalvarAssociado() throws Exception {
        AssociadoOutputWrapper associadoOutputWrapper = AssociadoOutputWrapperFixture.get();
        AssociadoInputWrapper associadoInputWrapper = AssociadoInputWrapperFixture.get();

        Mockito.when(associadoService.save(any(AssociadoInputWrapper.class))).thenReturn(associadoOutputWrapper);

        mockMvc.perform(post("/api/v1/associado")
                        .content(mapper.writeValueAsString(associadoInputWrapper))
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON)
                        .characterEncoding("utf-8"))
                .andExpect(status().isOk())
                .andDo(print())
                .andExpect(jsonPath("$.associadoId", Matchers.is(1)))
                .andExpect(jsonPath("$.nome", Matchers.is("Associado")));

    }

}