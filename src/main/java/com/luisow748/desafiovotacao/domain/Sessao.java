package com.luisow748.desafiovotacao.domain;

import jakarta.persistence.*;
import lombok.*;

import java.time.LocalDateTime;

@Entity
@Setter
@Getter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "sessao")
public class Sessao {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer sessaoId;

    private Integer prazoDuracao;
    private Integer pautaId;

    private LocalDateTime inicioSessao;
    private LocalDateTime terminoSessao;

}
