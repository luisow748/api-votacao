package com.luisow748.desafiovotacao.resource.mapper;

import com.luisow748.desafiovotacao.domain.Sessao;
import com.luisow748.desafiovotacao.service.output.SessaoOutputWrapper;
import org.mapstruct.Mapper;

@Mapper(componentModel="spring")
public interface SessaoMapper {

    SessaoOutputWrapper toOutputWrapper(Sessao sessao);
}
