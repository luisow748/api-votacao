<h1 > Desafio Votação </h1> <br>
<p>

</p>

<p>
  Projeto que simula o cadastro de associados, pautas, sessões e votações.
</p>



## Índice

- [Instalar](#instalação)
- [Utilização](#utilização)
- [Endpoints](#exemplos-de-chamadas)

## Instalação

Requisitos: Java 22 e Docker instalados.

No prompt, acesse a pasta do projeto e digite:

1- Aplicação:


```bash
$ .\gradlew bootRun
```


## Utilização

É possível realizar chamadas REST para as seguintes funcionalidades:
* Cadastrar Pauta.
* Cadastrar Sessão.
* Cadastrar Associado.
* Votar.
* Contabilizar Resultados.

## Exemplos de chamadas

1. Cadastrar Pauta:
   ```bash
      curl --location 'http://localhost:8080/api/v1/pauta' \
        --header 'Content-Type: application/json' \
        --data '{
        "nome": "pauta de teste 2",
        "descricao": "descrição de teste"
        }'

      ```
2. Cadastrar Sessão:

   ```bash
       curl --location 'http://localhost:8080/api/v1/sessao' \
      --header 'Content-Type: application/json' \
      --data '{
      "pautaId": 1,
      "prazoDuracao": 5
      }'

      ```
3. Cadastrar Associado:

   ```bash
       curl --location 'http://localhost:8080/api/v1/associado' \
       --header 'Content-Type: application/json' \
       --data '{
       "nome": "associado 1"
       }'
      ```

4. Cadastrar Voto:
   ```bash
       curl --location 'http://localhost:8080/api/v1/voto' \
       --header 'Content-Type: application/json' \
       --data '{
       "valor": "Nao",
       "sessaoId": 1,
       "pautaId": 1,
       "associadoId": 3
       }'

      ```
