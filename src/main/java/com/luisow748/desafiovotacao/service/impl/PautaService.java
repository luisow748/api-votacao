package com.luisow748.desafiovotacao.service.impl;

import com.luisow748.desafiovotacao.domain.Pauta;
import com.luisow748.desafiovotacao.exception.RecursoInexistenteException;
import com.luisow748.desafiovotacao.repository.PautaRepository;
import com.luisow748.desafiovotacao.resource.mapper.PautaMapper;
import com.luisow748.desafiovotacao.service.input.PautaInputWrapper;
import com.luisow748.desafiovotacao.service.output.PautaOutputWrapper;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class PautaService {
    private final PautaRepository pautaRepository;
    private final PautaMapper pautaMapper;

    public PautaOutputWrapper save(PautaInputWrapper pauta) {
        return pautaMapper.toOutputWrapper(save(pautaMapper.toEntity(pauta)));
    }

    public Pauta save(Pauta pauta) {
        return pautaRepository.save(pauta);
    }

    public Pauta findById(Integer pautaId) {
        return pautaRepository.findById(pautaId)
                .orElseThrow(() -> new RecursoInexistenteException("Pauta inexistente"));
    }
}
