package com.luisow748.desafiovotacao.fixture.wrapper;

import com.luisow748.desafiovotacao.domain.Voto;
import com.luisow748.desafiovotacao.domain.enums.ValorVotoEnum;
import com.luisow748.desafiovotacao.service.output.ContabilizacaoOutputWrapper;

import java.util.List;

import static com.luisow748.desafiovotacao.service.impl.VotoService.PAUTA_APROVADA;
import static com.luisow748.desafiovotacao.service.impl.VotoService.getQuantidadeVotos;

public class ContabilizacaoOutputWrapperFixture {
    public static ContabilizacaoOutputWrapper getFromListVotos(List<Voto> votos) {
        return ContabilizacaoOutputWrapper.builder()
                .votosSim(getQuantidadeVotos(votos, ValorVotoEnum.SIM))
                .votosNao(getQuantidadeVotos(votos, ValorVotoEnum.NAO))
                .resultado(PAUTA_APROVADA)
                .build();
    }
}
