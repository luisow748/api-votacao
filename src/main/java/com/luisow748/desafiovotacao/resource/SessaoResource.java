package com.luisow748.desafiovotacao.resource;


import com.luisow748.desafiovotacao.service.impl.SessaoService;
import com.luisow748.desafiovotacao.service.input.SessaoInputWrapper;
import com.luisow748.desafiovotacao.service.output.SessaoOutputWrapper;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("api/v1/sessao")
@AllArgsConstructor
public class SessaoResource {
    private final SessaoService sessaoService;

    @PostMapping
    ResponseEntity<SessaoOutputWrapper> iniciarSessao(@RequestBody SessaoInputWrapper inputWrapper){
        return ResponseEntity.ok(sessaoService.iniciarSessao(inputWrapper));
    }
}
