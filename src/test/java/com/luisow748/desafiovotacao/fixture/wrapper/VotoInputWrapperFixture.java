package com.luisow748.desafiovotacao.fixture.wrapper;

import com.luisow748.desafiovotacao.service.input.VotoInputWrapper;

public class VotoInputWrapperFixture {

    public static VotoInputWrapper get(String valorVoto) {
        VotoInputWrapper votoInputWrapper = new VotoInputWrapper();
        votoInputWrapper.setAssociadoId(1);
        votoInputWrapper.setSessaoId(1);
        votoInputWrapper.setPautaId(1);
        votoInputWrapper.setValor(valorVoto);
        return votoInputWrapper;
    }
}
