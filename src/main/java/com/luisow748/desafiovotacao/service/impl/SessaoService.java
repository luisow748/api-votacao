package com.luisow748.desafiovotacao.service.impl;

import com.luisow748.desafiovotacao.domain.Sessao;
import com.luisow748.desafiovotacao.exception.RecursoInexistenteException;
import com.luisow748.desafiovotacao.repository.SessaoRepository;
import com.luisow748.desafiovotacao.resource.mapper.SessaoMapper;
import com.luisow748.desafiovotacao.service.input.SessaoInputWrapper;
import com.luisow748.desafiovotacao.service.output.SessaoOutputWrapper;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;

@Service
@RequiredArgsConstructor
public class SessaoService {
    private final SessaoRepository sessaoRepository;
    private final SessaoMapper sessaoMapper;

    public SessaoOutputWrapper iniciarSessao(SessaoInputWrapper inputWrapper){
        Sessao sessaoIniciada = iniciar(inputWrapper);
        return sessaoMapper.toOutputWrapper(sessaoIniciada);
    }

    public Sessao iniciar(SessaoInputWrapper inputWrapper) {
        var inicioSessao = LocalDateTime.now();
        var prazoDuracao = Math.max(inputWrapper.getPrazoDuracao(), 1);

        Sessao sessaoIniciada = Sessao.builder()
                .prazoDuracao(prazoDuracao)
                .pautaId(inputWrapper.getPautaId())
                .inicioSessao(inicioSessao)
                .terminoSessao(inicioSessao.plusMinutes(prazoDuracao))
                .build();
        return sessaoRepository.save(sessaoIniciada);
    }

    public Sessao findById(Integer sessaoId) {
        return sessaoRepository.findById(sessaoId)
                .orElseThrow(() -> new RecursoInexistenteException("Sessão Inexistente"));
    }

    public Boolean isValid(Integer sessaoId) {
        var horarioAtual = LocalDateTime.now();
        return sessaoRepository.findById(sessaoId)
                .filter(sessao -> sessao.getTerminoSessao().isAfter(horarioAtual))
                .isPresent();
    }
}
