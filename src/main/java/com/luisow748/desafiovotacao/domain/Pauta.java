package com.luisow748.desafiovotacao.domain;

import jakarta.persistence.*;
import lombok.*;

@Entity
@Getter
@Setter
@Builder
@Table(name = "pauta")
@AllArgsConstructor
@NoArgsConstructor
public class Pauta {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer pautaId;

    private String nome;
    private String descricao;
    @ManyToOne
    @JoinColumn(name = "sessaoId")
    private Sessao sessao = null;

}
